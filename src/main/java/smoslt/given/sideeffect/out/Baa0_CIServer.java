/*Copyright 2014 Pete Carapetyan
 This file is part of smoslt.given module, one of many modules that belongs to smoslt

 smoslt.given is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 smoslt.given is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with smoslt.given in the file smoslt.given/GNUGeneralPublicLicensev3.0.html  
 If not, see <http://www.gnu.org/licenses/>.
 */
package smoslt.given.sideeffect.out;

import smoslt.domain.ScoreKey;
import smoslt.domain.ScoreTracker;
import smoslt.domain.SideEffectBase;
import smoslt.domain.Task;

public class Baa0_CIServer extends SideEffectBase {
	static boolean initialized = false;
	static boolean setupRun = false;

	public void go(ScoreTracker scoreTracker, Task task) {
		initializeSaturationLimits();
		if (task.getName().startsWith("DevImpl")) {
			setup(task);
			task.modifyDuration(.95);
		}
		scoreTracker.increment(ScoreKey.ManagerSpeak,
				3 * task.getDurationHours());
		scoreTracker.increment(ScoreKey.RDD, 3 * task.getDurationHours());
		scoreTracker.increment(ScoreKey.ScalesUp, 3 * task.getDurationHours());
		scoreTracker.increment(ScoreKey.LongTerm, task.getDurationDays());
	}

	private void setup(Task task) {
		if (!setupRun) {
			Task nutask = new Task(40, task.getStartDay(),
					"DevImpl: SETUP_FOR_" + task.getName(), "1 dev");
			setupRun = true;
		}
	}

	private void initializeSaturationLimits() {
		if (!initialized) {
			initializeSaturationLimit(ScoreKey.FeedbackSpeed, 30);
			incrementSaturationLimit(ScoreKey.FeedbackSpeed,
					getSaturationLimit(ScoreKey.FeedbackSpeed));
			initializeSaturationLimit(ScoreKey.RDD, 5);
			incrementSaturationLimit(ScoreKey.RDD,
					getSaturationLimit(ScoreKey.RDD));
			initializeSaturationLimit(ScoreKey.POLR, 10);
			incrementSaturationLimit(ScoreKey.POLR,
					getSaturationLimit(ScoreKey.POLR));
			initialized = true;
		}
	}

}
