/*Copyright 2014 Pete Carapetyan
 This file is part of smoslt.given module, one of many modules that belongs to smoslt

 smoslt.given is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 smoslt.given is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with smoslt.given in the file smoslt.given/GNUGeneralPublicLicensev3.0.html  
 If not, see <http://www.gnu.org/licenses/>.
 */
package smoslt.given.sideeffect.out;

import smoslt.domain.ScoreKey;
import smoslt.domain.ScoreTracker;
import smoslt.domain.SideEffectBase;
import smoslt.domain.Task;

public class Ahr_BestExampeSoFar extends SideEffectBase {

	public void go(ScoreTracker scoreTracker, Task task) {
		initializeSaturationLimit(ScoreKey.RDD, 5);
		initializeSaturationLimit(ScoreKey.ManagerSpeak, 5);
		initializeSaturationLimit(ScoreKey.POLR, 5);
		switch (task.getWorkRole()) {
		case "DevImpl":
			if (isFirstTaskWith(task.getWorkRole())) { // for "DevImpl" only
				new Task(4, task.getStartDay(),
						"DevImpl: Design arguments made for " + task.getName(),
						"2 dev");
				incrementSaturationLimit(ScoreKey.RDD, MAX);
				task.addDuration(1.05);
			} else { // for every "DevImpl" task
				task.modifyDuration(1.05);// modify number of hours by... 5%
			}
			break;// do not remove break!
		case "Foo":
			break;// do not remove break!
		case "BusAnlyst":
			if (isFirstTaskWith(task.getWorkRole())) { // for "BusAnlyst" only
				// break;
			} else { // for every "BusAnlyst" task
				task.modifyDuration(9999);// modify number of hours by...
			}
			break;// do not remove break!

		}

	}

}
