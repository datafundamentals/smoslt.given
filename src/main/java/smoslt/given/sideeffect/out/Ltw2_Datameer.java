/*Copyright 2014 Pete Carapetyan
 This file is part of smoslt.given module, one of many modules that belongs to smoslt

 smoslt.given is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 smoslt.given is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with smoslt.given in the file smoslt.given/GNUGeneralPublicLicensev3.0.html  
 If not, see <http://www.gnu.org/licenses/>.
 */
package  smoslt.given.sideeffect.out;

import smoslt.domain.ScoreKey;
import smoslt.domain.ScoreTracker;
import smoslt.domain.SideEffectBase;
import smoslt.domain.Task;

public class Ltw2_Datameer extends Bbf0_AutomatedBuild {
	boolean isInitialized = false;

	public void go(ScoreTracker scoreTracker, Task task) {
		super.go(scoreTracker, task);
		if (!isInitialized) {
			scoreTracker.increment(ScoreKey.ManagerSpeak, 150);
			scoreTracker.increment(ScoreKey.POLR, 5);
			scoreTracker.increment(ScoreKey.LongTerm, 100);
			scoreTracker.increment(ScoreKey.RDD, 5);
			isInitialized = true;
		}
		if (task.getName().startsWith("DevReport")) {
			task.modifyDuration(1.2);
		}
	}
}
