/*Copyright 2014 Pete Carapetyan
 This file is part of smoslt.given module, one of many modules that belongs to smoslt

 smoslt.given is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 smoslt.given is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with smoslt.given in the file smoslt.given/GNUGeneralPublicLicensev3.0.html  
 If not, see <http://www.gnu.org/licenses/>.
 */
package  smoslt.given.sideeffect.out;

import smoslt.domain.ScoreTracker;
import smoslt.domain.SideEffectBase;
import smoslt.domain.Task;

public class Lpx_ExampleFour extends SideEffectBase {

	public void go(ScoreTracker scoreTracker, Task task) {
		if (task.getName().startsWith("DevImpl")) {
			Task nutask = new Task(6, 200, "DevImpl: Some radical task", "1 dev");
			scoreTracker.getSchedule().getTaskList().add(nutask);
		}
		if (task.getName().startsWith("DevDoc")) {
			task.modifyDuration(3.3D);
		}
		if (task.getName().startsWith("DevDesign")) {
			task.modifyDuration(.8D);
		}
	}

}
