/*Copyright 2014 Pete Carapetyan
 This file is part of smoslt.given module, one of many modules that belongs to smoslt

 smoslt.given is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 smoslt.given is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with smoslt.given in the file smoslt.given/GNUGeneralPublicLicensev3.0.html  
 If not, see <http://www.gnu.org/licenses/>.
 */
package  smoslt.given.sideeffect;

import smoslt.domain.SideEffect;

public class SideEffectFactory {
	private static SideEffectFactory instance = null;

	public static SideEffectFactory getInstance() {
		if (null == instance) {
			instance = new SideEffectFactory();
		}
		return instance;
	}

	public SideEffect getSideEffect(String name) {
		if (!name.startsWith("smoslt.given.sideeffect.")) {
			name = "smoslt.given.sideeffect.in." + name;
		}
		Class newObject = null;
		SideEffect sideEffect = null;
		try {
			newObject = instance.getClass().getClassLoader().loadClass(name);
			sideEffect = (SideEffect) newObject.newInstance();
		} catch (ClassNotFoundException e) {
			throw new RuntimeException(
					"Unable to instantiate side effect class named " + name
							+ "\n" + e);
		} catch (InstantiationException e) {
			throw new RuntimeException(
					"Unable to instantiate side effect class named " + name
							+ "\n" + e);
		} catch (IllegalAccessException e) {
			e.printStackTrace();
			throw new RuntimeException(
					"Unable to instantiate side effect class named " + name
							+ "\n" + e);
		}
		return sideEffect;
	}

}
