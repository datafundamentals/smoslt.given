/*Copyright 2014 Pete Carapetyan
 This file is part of smoslt.given module, one of many modules that belongs to smoslt

 smoslt.given is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 smoslt.given is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with smoslt.given in the file smoslt.given/GNUGeneralPublicLicensev3.0.html  
 If not, see <http://www.gnu.org/licenses/>.
 */
package smoslt.given;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import smoslt.domain.FileConstants;
import smoslt.domain.SideEffect;
import smoslt.util.ConfigProperties;

public class SideEffectListImpl implements SideEffectList {

	@Override
	public List<String> get() {
		List<String> sideEffectList = new ArrayList<String>();
		File file = new File(ConfigProperties.get().getProperty(
				ConfigProperties.SIDE_EFFECT_IDE_COMPILE_DIR_CD)
				+ "/" + "in");
		if (!file.exists()) {
			file = new File(ConfigProperties.get().getProperty(
					ConfigProperties.SIDE_EFFECT_IDE_COMPILE_DIR_LOCAL)
					+ "/" + "in");
		}
		if (!file.exists()) {

			throw new RuntimeException(
					"SideEffectList hard coded to run in IDE needs more work this file does not exist "
							+ file.getAbsolutePath());
		}
		if (file.exists()) {
			for (File name : file.listFiles()) {
				String fileName = name.getName();
				if (fileName.endsWith(".class")) {
					System.out.println(fileName);
					fileName = fileName.substring(0, fileName.length() - 6);
					if (!fileName.equals("SideEffectFactory")) {
						sideEffectList.add(fileName);
					}
				}
			}
		}
		return sideEffectList;
	}

	// public String getSideEffectsListHeaders(){
	// StringBuffer sb = new StringBuffer();
	// for(String header:get()){
	// sb.append(header+",");
	// }
	// return sb.toString();
	// }

}
