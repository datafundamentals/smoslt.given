/*Copyright 2014 Pete Carapetyan
 This file is part of smoslt.given module, one of many modules that belongs to smoslt

 smoslt.given is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 smoslt.given is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with smoslt.given in the file smoslt.given/GNUGeneralPublicLicensev3.0.html  
 If not, see <http://www.gnu.org/licenses/>.
 */
package smoslt.given;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import smoslt.domain.FileConstants;
import smoslt.domain.SideEffect;

public class SideEffectListTestHarness implements SideEffectList {

	@Override
	public List<String> get() {
		List<String> sideEffectList = new ArrayList<String>();
		sideEffectList.add("Abk_AbleWillingDiscussDesignIntelligently");
		sideEffectList.add("Abt_SticksToPlan");
		sideEffectList.add("Baa3_Jenkins");
		sideEffectList.add("Bbf1_Maven");
		sideEffectList.add("Bbj2_DataFundamentalsEtlTooling");
		sideEffectList.add("Bbu1_Chef");
		sideEffectList.add("Ctr_InvolvesOracleSalesTeam");
		sideEffectList.add("Dbs1_Kafka");
		sideEffectList.add("Dhr_ExampleThree");
		sideEffectList.add("Kuc_ExampleOne");
		sideEffectList.add("Lpx_ExampleFour");
		sideEffectList.add("Tdq_ExampleTwo");
		return sideEffectList;
	}

}
