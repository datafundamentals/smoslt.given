package smoslt.given;

import java.util.List;

public interface SideEffectList {
	public List<String> get();
}
