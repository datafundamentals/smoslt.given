/*Copyright 2014 Pete Carapetyan
 This file is part of smoslt.given module, one of many modules that belongs to smoslt

 smoslt.given is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 smoslt.given is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with smoslt.given in the file smoslt.given/GNUGeneralPublicLicensev3.0.html  
 If not, see <http://www.gnu.org/licenses/>.
 */
package  smoslt.given;

import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import org.btrg.uti.FileAsList_;

public class ParseDelimitedFile {

	public List<List<String>> get(String filePath) {
		List<String> lineList = FileAsList_
				.read(filePath);
		List<List<String>> fields = new ArrayList<List<String>>();
		for (String line : lineList) {
			fields.add(getLineContents(line));
		}
		return fields;
	}

	private List<String> getLineContents(String line) {
		List<String> fieldList = new ArrayList<String>();
		StringTokenizer stk = new StringTokenizer(line, ",");
		int position = 0;
		String firstField = null;
		while (stk.hasMoreTokens()) {
			String field = stk.nextToken();
			if (position == 0) {
				firstField = field;
				position++;
			} else {
				fieldList.add(field);
			}
		}
		fieldList.addAll(getBooleanValues(firstField));
//		print(fieldList);  
		return fieldList;
	}

	private void print(List<String> fieldList) {
		System.out.print("FIELD LIST: ");
		for (String value : fieldList) {
			System.out.print(value + " ");
		}
		System.out.println();
	}

	private List<String> getBooleanValues(String fieldValue) {
		List<String> booleanList = new ArrayList<String>();
		char aChar;
		for (int i = 0; i < fieldValue.length();) {
			aChar = fieldValue.charAt(i++);
			switch (aChar) {
			case '0':
				booleanList.add("false");
				break;
			case '1':
				booleanList.add("true");
				break;
			default:
				throw new IllegalArgumentException(
						"Should only have parsed 1 or 0, instead found '"
								+ aChar + "'");
			}
		}
		return booleanList;
	}
}