/*Copyright 2014 Pete Carapetyan
 This file is part of smoslt.given module, one of many modules that belongs to smoslt

 smoslt.given is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 smoslt.given is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with smoslt.given in the file smoslt.given/GNUGeneralPublicLicensev3.0.html  
 If not, see <http://www.gnu.org/licenses/>.
 */
package smoslt.given;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import smoslt.domain.Schedule;
import smoslt.domain.ScoreKey;
import smoslt.domain.ScoreTracker;

public class ScoreTrackerImpl implements ScoreTracker {
	private Schedule schedule;
	private Map<ScoreKey, Integer> scores = new LinkedHashMap<ScoreKey, Integer>();

	private ScoreTrackerImpl() {
	}

	public ScoreTrackerImpl(Schedule schedule) {
		this.schedule = schedule;
		// FAIL, then Hours must always be there, and be first two in the list
		scores.put(ScoreKey.FAIL, 0);
		scores.put(ScoreKey.Hours, 0);
		// everything below here can be optional. More scores do not necessarily
		// slow things down noticeably
		scores.put(ScoreKey.FeedbackSpeed, 0);
		scores.put(ScoreKey.RDD, 0);
		scores.put(ScoreKey.ManagerSpeak, 0);
		scores.put(ScoreKey.ScalesUp, 0);
		scores.put(ScoreKey.POLR, 0);
		scores.put(ScoreKey.LongTerm, 0);
	}

	@Override
	public Schedule getSchedule() {
		return schedule;
	}

	@Override
	public void increment(ScoreKey scoreKey, int increment) {
		int value = scores.get(scoreKey);
		value += increment;
		scores.put(scoreKey, value);
	}

	@Override
	public int getScore(ScoreKey key) {
		return scores.get(key);
	}

	@Override
	public List<Integer> getScores() {
		List<Integer> scoreList = new ArrayList<Integer>();
		scoreList.addAll(scores.values());
		return scoreList;
	}

	@Override
	public List<ScoreKey> getHeaders() {
		List<ScoreKey> headers = new ArrayList<ScoreKey>();
		headers.addAll(scores.keySet());
		return headers;
	}

}
