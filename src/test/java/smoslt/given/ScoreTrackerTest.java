/*Copyright 2014 Pete Carapetyan
 This file is part of smoslt.given module, one of many modules that belongs to smoslt

 smoslt.given is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 smoslt.given is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with smoslt.given in the file smoslt.given/GNUGeneralPublicLicensev3.0.html  
 If not, see <http://www.gnu.org/licenses/>.
 */
package  smoslt.given;

//import static org.junit.Assert.*;
import static org.junit.Assert.*;

import java.util.List;

import org.junit.Before;
import org.junit.Test;

import smoslt.domain.ScoreKey;

public class ScoreTrackerTest {
	ScoreTrackerImpl scoreTracker = new ScoreTrackerImpl(null);

	@Before
	public void setUp() {
		scoreTracker.increment(ScoreKey.FAIL, 0);
		scoreTracker.increment(ScoreKey.Hours, 1);
		scoreTracker.increment(ScoreKey.FeedbackSpeed, 2);
		scoreTracker.increment(ScoreKey.RDD, 3);
		scoreTracker.increment(ScoreKey.ManagerSpeak, 4);
		scoreTracker.increment(ScoreKey.ScalesUp, 5);
		scoreTracker.increment(ScoreKey.POLR, 6);
		scoreTracker.increment(ScoreKey.LongTerm, 7);
	}

	/*
	 * You want to test that the order has not changed
	 */
	@Test
	public void testScoreIncrement() {
		// scoreTracker.increment(ScoreKey., 0);
		List<Integer> scores = scoreTracker.getScores();
		for (int i = 1; i < 8; i++) {
			assertEquals(i, scores.get(i).intValue());
		}
	}

	/*
	 * You want to test that the order has not changed
	 */
	@Test
	public void testScoreHeaderOrder() {
		List<ScoreKey> scoreHeaders = scoreTracker.getHeaders();
		assertEquals(ScoreKey.FAIL, scoreHeaders.get(0));
		assertEquals(ScoreKey.Hours, scoreHeaders.get(1));
		assertEquals(ScoreKey.FeedbackSpeed, scoreHeaders.get(2));
		assertEquals(ScoreKey.RDD, scoreHeaders.get(3));
		assertEquals(ScoreKey.ManagerSpeak, scoreHeaders.get(4));
		assertEquals(ScoreKey.ScalesUp, scoreHeaders.get(5));
		assertEquals(ScoreKey.POLR, scoreHeaders.get(6));
		assertEquals(ScoreKey.LongTerm, scoreHeaders.get(7));
	}
	@Test
	public void testGetScore() {
		assertEquals( scoreTracker.getScore(ScoreKey.FAIL), 0);
		assertEquals(scoreTracker.getScore(ScoreKey.Hours), 1);
		assertEquals(scoreTracker.getScore(ScoreKey.FeedbackSpeed), 2);
		assertEquals(scoreTracker.getScore(ScoreKey.RDD), 3);
		assertEquals(scoreTracker.getScore(ScoreKey.ManagerSpeak), 4);
		assertEquals(scoreTracker.getScore(ScoreKey.ScalesUp), 5);
		assertEquals(scoreTracker.getScore(ScoreKey.POLR), 6);
		assertEquals(scoreTracker.getScore(ScoreKey.LongTerm), 7);
	}
	
	

}
